package mvp;
import negocio.Grafo;
import negocio.Logica;

import java.util.List;

import negocio.ConjuntoDomMin;


@SuppressWarnings("unused")
public class Presenter {

	public static int tamanioGrafo() {
		
		return Logica.tamanio();
	}
	public static void CrearGrafo(int vertices){
		Logica.crearGrafo(vertices);
	}
	public static void AgregarArista(int vertice1, int vertice2){
		Logica.AgregarArista(vertice1, vertice2);
	}
	
	
	public static Grafo verGrado() {
		return Logica.verGrado();
	}
	
	public static int retornarArista(int x,int y){
		return Logica.retornarArista(x, y);
	}
	public static int cantdearitas() {
		// TODO Auto-generated method stub
		return Logica.catintidad_de_aritas();
	}
	public static List<Integer> ConjuntoDominateMinimo() {
		//return Logica.ConjuntoDominateMinimo();
		return Logica.ConjuntoDominate();
	}
	
	
}
