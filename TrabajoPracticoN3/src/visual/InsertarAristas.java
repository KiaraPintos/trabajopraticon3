package visual;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;

import mvp.Presenter;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InsertarAristas {

	private JFrame frame;
	private JTextField PrimerVertice;
	private JTextField SegundoVertice;
	private JButton btnAgregar;
	private JButton btnResultado_1;
	private JButton btnVerGrafo;
	protected int Vertice1;
	protected int Vertice2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InsertarAristas window = new InsertarAristas();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void limpiarEntrada() {
		PrimerVertice.setText("");
		SegundoVertice.setText("");
	}
	private void incializarPanel() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 250));
		frame.setBounds(100, 100, 473, 343);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel titulo = new JLabel("Agregar Aristas");
		titulo.setFont(new Font("Times New Roman", Font.PLAIN, 29));
		titulo.setBounds(122, 0, 203, 60);
		frame.getContentPane().add(titulo);

		PrimerVertice = new JTextField();
		PrimerVertice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		PrimerVertice.setBounds(186, 89, 75, 39);
		frame.getContentPane().add(PrimerVertice);
		PrimerVertice.setColumns(10);

		JLabel lblColoquePrimerVertice = new JLabel("Coloque el Primer vertice");
		lblColoquePrimerVertice.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblColoquePrimerVertice.setBounds(10, 77, 203, 60);
		frame.getContentPane().add(lblColoquePrimerVertice);

		JLabel lblColoqueSegundoVertice_1 = new JLabel("Coloque el Segundo vertice");
		lblColoqueSegundoVertice_1.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblColoqueSegundoVertice_1.setBounds(10, 141, 203, 60);
		frame.getContentPane().add(lblColoqueSegundoVertice_1);

		SegundoVertice = new JTextField();
		SegundoVertice.setFont(new Font("Tahoma", Font.PLAIN, 18));
		SegundoVertice.setColumns(10);
		SegundoVertice.setBounds(186, 153, 75, 39);
		frame.getContentPane().add(SegundoVertice);

	}
	private void CrearBotones(){
		btnAgregar = new JButton("Agregar");

		btnAgregar.setBounds(30, 252, 99, 21);
		frame.getContentPane().add(btnAgregar);


		btnResultado_1 = new JButton("Resultados");
		btnResultado_1.setBounds(326, 251, 99, 23);
		frame.getContentPane().add(btnResultado_1);

		btnVerGrafo = new JButton("Ver Grafo");
		btnVerGrafo.setBounds(170, 252, 99, 21);
		frame.getContentPane().add(btnVerGrafo);

		JLabel lblNewLabel = new JLabel("Recorda que las aristas van de 0 a " + (Presenter.tamanioGrafo()-1));
		lblNewLabel.setBounds(30, 53, 322, 14);
		frame.getContentPane().add(lblNewLabel);
	}
	private void escucharBotonAgregar() {
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					if (PrimerVertice.getText().equals("") || SegundoVertice .getText().equals("")) {
						JOptionPane.showMessageDialog(null, "Debe poner los vertices");
					}else if(!PrimerVertice.getText().equals("") || !SegundoVertice .getText().equals("")) {
						Presenter.AgregarArista(Integer.parseInt(PrimerVertice.getText()),Integer.parseInt(SegundoVertice .getText()));
						limpiarEntrada();	
						;}} catch (Exception e1) {JOptionPane.showMessageDialog(null, "no se permiten loops o nuemeros mayores al tamaño del grafo");}}});
	}

	private void escucharBotonVerGrafo() {
		btnVerGrafo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VerGrafo.main(null);
			}
		});
	}
	private void escucharBotonResultados() {
		btnResultado_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {	
					if (Presenter.cantdearitas()<3) {
						JOptionPane.showMessageDialog(null, "Debe tener al menos 3 aristas");
					}else  {
						Resultados.main(null);
						frame.dispose();
						;}} catch (Exception e1) {JOptionPane.showMessageDialog(null, "no se permiten loops o nuemeros mayores al tamaño del grafo");}}});}

	/**¨´
	 * Create the application.
	 */



	public InsertarAristas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		CrearBotones();
		escucharBotonAgregar();
		escucharBotonVerGrafo();
		escucharBotonResultados();

	}
}
