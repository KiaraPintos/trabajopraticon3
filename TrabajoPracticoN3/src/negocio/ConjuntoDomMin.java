package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConjuntoDomMin {
    public static void main(String[] args) {
        int[] arreglo = {0, 5, 6, 2, 3, 7, 9, 8, 4, 1};
        List<Integer> indices = new ArrayList<>();

        // Llenar la lista de índices con los valores del 0 al tamaño del arreglo - 1
        for (int i = 0; i < arreglo.length; i++) {
            indices.add(i);
        }

        // Ordenar la lista de índices en función de los valores correspondientes en el arreglo
        Collections.sort(indices, (a, b) -> Integer.compare(arreglo[b], arreglo[a]));

        // Mostrar la lista de índices ordenada
        System.out.println("Lista de índices ordenada por valores del arreglo: " + indices);
        
        // Mostrar la lista original
        System.out.println("Lista original: " + indices);

        System.out.println("Primer numero: " + indices.get(0));
        // Eliminar el elemento que contiene el número 8
        int numeroAEliminar1 = 6;
        indices.remove(Integer.valueOf(numeroAEliminar1));

        // Mostrar la lista después de eliminar el número 8
        System.out.println("Lista después de eliminar el número 8: " + indices);
        System.out.println("Primer numero: " + indices.get(0));
        // Eliminar el elemento que contiene el número 5
        int numeroAEliminar2 = 7;
        indices.remove(Integer.valueOf(numeroAEliminar2));

        // Mostrar la lista después de eliminar el número 5
        System.out.println("Lista después de eliminar el número 5: " + indices);
        
        System.out.println("Primer numero: " + indices.get(0));
    }
}
