package visual;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import mvp.Presenter;


public class Resultados {


	private JFrame frame;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnCerra;
	private String cadena;
	private List<Integer> list;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Resultados window = new Resultados();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	private  void repuesta() {
		list=Presenter.ConjuntoDominateMinimo();
		cadena = "Resultado:\n";
		cadena += "{";
		for(int i=0; i<list.size();i++) {
			cadena+=list.get(i);
			cadena += ";";

		}
		cadena += "}";
		cadena += "\n";
		textArea.setText(cadena);
	}	    


	private void incializarPanel(){
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 61, 434, 143);
		frame.getContentPane().add(scrollPane);

		textArea = new JTextArea();
		textArea.setEnabled(false);
		textArea.setEditable(false);
		textArea.setForeground(new Color(0, 0, 0));
		textArea.setFont(new Font("Times New Roman", Font.BOLD, 19));
		scrollPane.setViewportView(textArea);

		JLabel lblNewLabel = new JLabel("Resultados");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 19));
		lblNewLabel.setBounds(145, 11, 191, 40);
		frame.getContentPane().add(lblNewLabel);

		btnCerra = new JButton("Cerrar");
		btnCerra.setBounds(188, 227, 89, 23);
		frame.getContentPane().add(btnCerra);
	}
	private void escucharBotonCerrar() {
		btnCerra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
	}
	/**
	 * Create the application.
	 */
	public Resultados() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		escucharBotonCerrar() ;
		repuesta();
	}

}
