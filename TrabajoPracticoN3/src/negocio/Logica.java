package negocio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Logica {
	private static Grafo Grafo_G;
	private static int cantAritas;
	private static List<Integer> CantVecionos = new ArrayList<>();
	private static List<Integer> componente = new ArrayList<>();
	private static ArrayList<Integer> verticeycantvecinos;
	private static int[] arrayConCantVecinos;
	private static ArrayList<Integer> arrayAuxiliar=new ArrayList<>();
	private static List<Integer> listaDeVerticesDeMayorAMenor = new ArrayList<>();
	private static ArrayList<Integer> VerticesVisitados;

	public static void crearGrafo(int vertices) {
		Grafo_G=new Grafo(vertices);
		arrayConCantVecinos=new int[vertices];
		System.out.println("Largo del array:"+arrayConCantVecinos.length);

	}

	public static void AgregarArista(int vertice1, int vertice2) {
		Grafo_G.agregarArista(vertice1, vertice2);
	}

	public static int tamanio() {
		return Grafo_G.tamano();
	}

	public static Grafo verGrado() {
		for (int i = 0; i <  Grafo_G.tamano(); i++) {
			for (int j = 0; j <  Grafo_G.tamano(); j++) {
				System.out.print(Grafo_G.retornarArista(i, j));

			}  
			System.out.print( "\n");
		}	    
		return Grafo_G;
	}




	public static int retornarArista(int x,int y){
		return Grafo_G.retornarArista(x, y);
	}

	public static int catintidad_de_aritas() {
		for (int i = 0; i <  Grafo_G.tamano(); i++) {
			for (int j = 0; j <  Grafo_G.tamano(); j++) {
				if(Grafo_G.retornarArista(i, j)==1) {cantAritas=cantAritas+1;}  

			}	    	
		}
		return cantAritas;

	}

	public static int Verticeconmasvecios() {
		ListaVerticesYlaCantidadDeVecinos();
		int valorMax = Integer.MIN_VALUE;
		for (int i = 0; i <  verticeycantvecinos.size(); i++) {
			final int valorActual = verticeycantvecinos.get(i);
			if (valorActual > valorMax)
				valorMax = valorActual;
		}
		return verticeycantvecinos.indexOf(valorMax);

	}

	public static List<Integer> ListaVerticesYlaCantidadDeVecinos() {
		verticeycantvecinos=new ArrayList<>();
		for (int i = 0; i <  Grafo_G.tamano(); i++) {
			verticeycantvecinos.add(Grafo_G.vecinos(i).size());
		}
		return   verticeycantvecinos;
	}

	public static List<Integer> ConjuntoDominateMinimo(){
		List<Integer>  Componente=new ArrayList<>();
		verticesVicitados(Verticeconmasvecios());

		if(!Componente.contains( Verticeconmasvecios()) && VerticesVisitados.contains(Verticeconmasvecios())){
			Componente.add(Verticeconmasvecios());

		}
		if(!Componente.contains( Verticeconmasvecios()) && !VerticesVisitados.contains(Verticeconmasvecios())){
			Componente.add(Verticeconmasvecios());

		}


		return Componente;
	}

	public static void verticesVicitados(int i) {
		VerticesVisitados = new ArrayList<>();
		Set<Integer> ret = Grafo_G.vecinos(i);
		for(Integer i1 : ret ) {
			VerticesVisitados.add(i1);
		}
	}

	//Lucas------------------------------------------------------------

	public static List<Integer> ConjuntoDominate(){
		int vertice;
		cargarListaDeCantidades();
		ordenarListaSegunCantidadVecinos();
		while(arrayAuxiliar.size()<arrayConCantVecinos.length)
		{
			vertice=listaDeVerticesDeMayorAMenor.get(0);
			listaDeVerticesDeMayorAMenor.remove(Integer.valueOf(vertice));
			componente.add(vertice);
			agregarVerticeYVecinosAlAuxiliar(vertice);		
		}
		return componente;
	}
	
	private static void cargarListaDeCantidades() {
		for(int i=0;i<arrayConCantVecinos.length;i++) {
			arrayConCantVecinos[i]=Grafo_G.cantVecinos(i);
			listaDeVerticesDeMayorAMenor.add(i);
		}	
	}
	
	private static void ordenarListaSegunCantidadVecinos() {
		Collections.sort(listaDeVerticesDeMayorAMenor, (a, b) -> Integer.compare(arrayConCantVecinos[b], arrayConCantVecinos[a]));
	}
	
	private static void agregarVerticeYVecinosAlAuxiliar(int vertice) {
		arrayAuxiliar.add(vertice);
		for(Integer x : Grafo_G.vecinos(vertice)) {
			arrayAuxiliar.add(x);
			listaDeVerticesDeMayorAMenor.remove(Integer.valueOf(x));
		}
	}
	
	//------------------------------------------------------------------
}
	
	



