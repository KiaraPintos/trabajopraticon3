package visual;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import mvp.Presenter;
import negocio.Grafo;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VerGrafo {

	JFrame frame;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JButton btnCerra;
	private String cadena;
	private Grafo Grafo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerGrafo window = new VerGrafo();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	private  void repuesta() {
		Grafo = Presenter.verGrado();
		cadena = "el grafo en una matriz:\n";
		for (int i = 0; i <  Grafo.tamano(); i++) {
			for (int j = 0; j <  Grafo.tamano(); j++) {
				cadena += Presenter.retornarArista(i, j);

			}  
			cadena += "\n";
		}	    
		textArea.setText(cadena);
	}
	private void incializarPanel(){
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 61, 434, 143);
		frame.getContentPane().add(scrollPane);

		textArea = new JTextArea();
		textArea.setEnabled(false);
		textArea.setEditable(false);
		textArea.setForeground(new Color(0, 0, 0));
		textArea.setFont(new Font("Times New Roman", Font.BOLD, 19));
		scrollPane.setViewportView(textArea);

		JLabel lblNewLabel = new JLabel("Grafo Creado");
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 19));
		lblNewLabel.setBounds(145, 11, 191, 40);
		frame.getContentPane().add(lblNewLabel);

		btnCerra = new JButton("Cerrar");
		btnCerra.setBounds(188, 227, 89, 23);
		frame.getContentPane().add(btnCerra);
	}
	private void escucharBotonCerrar() {
		btnCerra.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
	}
	/**
	 * Create the application.
	 */
	public VerGrafo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		escucharBotonCerrar() ;
		repuesta();
	}

}
