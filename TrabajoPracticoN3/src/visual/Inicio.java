package visual;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import mvp.Presenter;

@SuppressWarnings("unused")
public class Inicio {

	private JFrame frame;
	private JTextField tamGrafo;
	private JButton btnRestar = new JButton("-");
	private JButton btnSumar = new JButton("+");
	private JButton btnContinuar = new JButton("CONTINUAR");
	private final JButton btnCrearGrafo_1 = new JButton("Crear Grafo");
	private final JButton btnVerGrafo_1 = new JButton("Ver Grafo");


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Inicio window = new Inicio();
					window.frame.setVisible(true);
					window.frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void incializarPanel() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(135, 206, 235));
		frame.setBounds(100, 100, 492, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);



		JLabel txtIndicacion = new JLabel("Tamaño del grafo");
		txtIndicacion.setFont(new Font("Tahoma", Font.PLAIN, 24));
		txtIndicacion.setHorizontalAlignment(SwingConstants.CENTER);
		txtIndicacion.setBounds(36, 11, 374, 41);
		frame.getContentPane().add(txtIndicacion);

		tamGrafo = new JTextField();
		tamGrafo.setText("4");
		tamGrafo.setHorizontalAlignment(SwingConstants.CENTER);
		tamGrafo.setBounds(189, 104, 64, 20);
		frame.getContentPane().add(tamGrafo);
		tamGrafo.setColumns(10);

		JPanel buttonPane = new JPanel();
		frame.getContentPane().add(buttonPane, BorderLayout.SOUTH);
		buttonPane.setLayout(null);
	}


	private void crearBotones() {
		btnSumar.setBounds(299, 103, 41, 23);
		frame.getContentPane().add(btnSumar);

		btnRestar.setBounds(98, 103, 41, 23);
		frame.getContentPane().add(btnRestar);	
		btnContinuar.setBounds(326, 205, 140, 23);
		frame.getContentPane().add(btnContinuar);
		btnCrearGrafo_1.setBounds(10, 205, 140, 23);
		frame.getContentPane().add(btnCrearGrafo_1);
		btnVerGrafo_1.setBounds(160, 205, 140, 23);
		frame.getContentPane().add(btnVerGrafo_1);
	}

	private void escucharBotonRestar() {
		btnRestar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {		
					int tam=Integer.parseInt(tamGrafo.getText());
					if(tam>4){
						tamGrafo.setText((--tam)+"");
					}

				} catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");}		
			}		
		});
	}



	private void escucharBotonSumar() {
		btnSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {		
					int tam=Integer.parseInt(tamGrafo.getText());
					tamGrafo.setText((++tam)+"");
				}

				catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");}		
			}		
		});
	}


	private void escucharBotonContinuar() {
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(Presenter.tamanioGrafo()==0)
					{throw new IllegalArgumentException("Debe crear el grafo para poder continuar");}
					else {InsertarAristas.main(null);frame.dispose();}}
				catch (Exception e) {JOptionPane.showMessageDialog(null, "Debe crear el grafo para poder continuar");}}});}

	private void escucharBotonCrearGrafo() {
		btnCrearGrafo_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					int tam=Integer.parseInt(tamGrafo.getText());
					Presenter.CrearGrafo(tam);
				}
				catch (Exception e) {JOptionPane.showMessageDialog(null, "ERROR");}		
			}		
		});
	}


	private void escucharBotonVerGrafo() {
		btnVerGrafo_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {


				try {
					if(Presenter.tamanioGrafo()==0) {
						JOptionPane.showMessageDialog(null, "Debe crear el grafo para poder verlo");
					}
					else {
						VerGrafo.main(null);
					}
				}catch (Exception e) {JOptionPane.showMessageDialog(null, "Debe crear el grafo para poder verlo");}		
			}		
		});
	}





	/**
	 * Create the application.
	 */
	public Inicio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		incializarPanel();
		crearBotones();
		escucharBotonRestar();
		escucharBotonSumar();
		escucharBotonCrearGrafo();
		escucharBotonContinuar();
		escucharBotonVerGrafo() ;


	}

}
